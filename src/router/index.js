import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'

import Login from '../containers/Login'
import Dashboard from '../containers/Dashboard'
import Banners from '../containers/Banners'
import Contacts from '../containers/Contacts'
import ContactView from '../containers/Contacts/View'
import News from '../containers/News'
import Subjects from '../containers/Subjects'
import Users from '../containers/Users'
import Complaints from '../containers/Complaints'
import ComplaintDetail from '../containers/Complaints/View'
import Ratings from '../containers/Ratings'

import PrivateRoute from './privateRoute'
import PublicRoute from './publicRoute'

const Router = () => (
	<BrowserRouter>
		<Switch>
			<PublicRoute component={false} path={'/'} exact />
			<PublicRoute component={Login} path={'/login'} />
			<PrivateRoute component={Dashboard} path={'/dashboard'} />
      		<PrivateRoute component={Banners} path={'/banners'} />
			<PrivateRoute component={ContactView} path={'/contacts/:id'} />
			<PrivateRoute component={Contacts} path={'/contacts'} />
			<PrivateRoute component={News} path={'/news'} />
			<PrivateRoute component={Subjects} path={'/subjects'} />
			{/* <PrivateRoute component={Users} path={'/users'} /> */}
			<PrivateRoute component={ComplaintDetail} path={'/complaints/:id'} />
			<PrivateRoute component={Complaints} path={'/complaints'} />
			<PrivateRoute component={Ratings} path={'/ratings'} />
		</Switch>
	</BrowserRouter>
)

export default Router