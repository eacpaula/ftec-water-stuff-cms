import { gql } from '@apollo/client'

const GET_DATA = gql`
  query {
    contacts(params: { term: "" }) {
      id
      subject_id
      fullname
      email
      cellphone
      message
      createdAt
      subject {
        id
        title
      }
    }
  }
`;

const GET_DATA_USERS = gql`
  query {
    users(params: { term: "" }) {
      id
      username
      createdAt
      updatedAt
    }
  }
`;

const queries = {
  GET_DATA,
  GET_DATA_USERS
}

export default queries