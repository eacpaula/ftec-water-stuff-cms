import React, { useState, Fragment } from 'react'
import { useHistory } from 'react-router-dom'
import { CircularProgress, Button } from '@material-ui/core'
import { useQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import Form from '../../components/Form'

export default function RegisterRequestsProtocols() {
  const history = useHistory()
  const initialState = {
    subtitle: "",
    description: ""
  }

  const schema = object().shape({
    subtitle: string(),
    description: string().required("A descrição é obrigatória!"),
    water_register_id: number(),
    request_protocol_register_id: number()
	})

  const inputs = [
    {
      type: "text",
      name: "subtitle",
      label: "Subtítulo",
      readOnly: true,
      props: {
        placeholder: "Subtítulo",
        style: ""
      }
    },
    {
      type: "text",
      name: "description",
      label: "Descrição",
      readOnly: true,
      props: {
        placeholder: "Descrição",
        style: ""
      }
    },
    {
      type: "hidden",
      name: "water_register_id"
    },
    {
      type: "hidden",
      name: "request_protocol_register_id"
    },
  ]

  const { loading } = useQuery( queries.GET_DATA, {
    onCompleted: (response) => {
      var data

      if(response && response.waterRegisterByUser) {
        const info = response.waterRegisterByUser;

        data = {
          ...info,
          water_register_id: info.id
        }
      }

      setState({
        ...state,
        data
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [state, setState] = useState({
    inputs,
    schema,
    initialState,
    data: {}
  })

  const mutation = { definition: mutations.ADD_DATA, name: 'addWaterRegisterContact' }

  return (
    <Content title={'Cadastrar denúncia'}>
      { loading ?
          <CircularProgress disableShrink />
        :
        <Fragment>
          <Form
            title="Cadastrar denúncia"
            description=""
            source={state.data}
            inputs={state.inputs}
            schema={state.schema}
            mutation={mutation}
            callback={(success, data) => {}}
            initialState={state.initialState} />
          <Button onClick={() => { history.push('/listing-requests-protocols') }} color="primary">
            Cancelar
          </Button>
        </Fragment>
      }
    </Content>
  )
}