import { React, useState, Fragment } from 'react'
import AWS from 'aws-sdk'
import mime from 'mime-types';
import { gql, useMutation } from '@apollo/client'

export default function UploadFile(props) {
	const { name, inputRef, value, handleOnChange, callback } = props

	const [mediaId, setMediaId] = useState(value)

	const UPLOAD = gql`
		mutation($data: FileS3!) {
			addMediaS3(data: $data) {
				id,
				path,
				filename, 
				filename_original,
				mimetype
			}
		}
	`

	const [mutate, { loading, error }] = useMutation(UPLOAD, {
		onCompleted: (data) => {
			callback({ success: true, ...data.addMediaS3 })
			setMediaId(data.addMediaS3.id)
			handleOnChange(data.addMediaS3.id)
		},
		onError: (error) => {
			console.log(error)
		}
	});

	const BUCKET = process.env.REACT_APP_AWS_BUCKET_S3;
	const REGION = process.env.REACT_APP_AWS_REGION;
	const ACCESS_KEY = process.env.REACT_APP_AWS_ACCESS_KEY;
	const SECRET_ACCESS_KEY = process.env.REACT_APP_AWS_SECRET_KEY;

	AWS.config.update({
		accessKeyId: ACCESS_KEY,
		secretAccessKey: SECRET_ACCESS_KEY
	});
	
	const bucket = new AWS.S3({
		params: { Bucket: BUCKET},
		region: REGION,
	});

	const uploadFile = (file) => {
		const filename_original = file.name;
		const extension = file.name.split('.')[1];
		const filename = `${Date.now()}.${extension}`;
		const mimetype = mime.lookup(file.name);
		const location = `https://${BUCKET}.s3.${REGION}.amazonaws.com/${filename}`

        const params = {
            ACL: 'public-read',
            Body: file,
            Bucket: BUCKET,
            Key: filename
        };

        bucket.putObject(params)
            // .on('httpUploadProgress', (evt) => {
            //     setProgress(Math.round((evt.loaded / evt.total) * 100))
            // })
            .send((err) => {
                if (err) console.log(err)
				
				const data = {
					filename,
					filename_original,
					extension,
					mimetype,
					location
				}

				mutate({ variables: { data } })
            })
    }
	
	const onChange = ({
		target: {
			validity,
			files: [file]
		}
	}) => {
		if(file && file.size > 5242880){
			callback({ success: false, message: `Arquivo ${file.name} ultrapassou o tamanho máximo de 5MB permitido` });
		} else {
			if(validity.valid)
				uploadFile(file);
		}
	};

	if (loading) return <div>Loading...</div>;
	if (error) return <div>{JSON.stringify(error, null, 2)}</div>;

	return (
		<Fragment>
			<input 
			  type="file" 
			  onChange={onChange} 
			  className="btn-upload" />
			<input 
				type="hidden" 
				ref={inputRef}
				name={name} 
				value={mediaId} />
		</Fragment>
	)
}