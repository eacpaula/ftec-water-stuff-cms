import moment from 'moment'
import { useHistory } from 'react-router-dom'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress, Button } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function ListingComplaints() {
  const history = useHistory()
  const [state, setState] = useState({
    columns: [
      { title: 'Descrição', field: 'description', render: data => 'Descrição da denúncia' },
      { title: 'Subtítulo', field: 'subtitle', render: data => 'Subtítulo da denúncia'},
    ],
    data: [],
    actions: []
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({
        columns: state.columns,
        data: data.waterRegisterHistoriesByUser.map(x => Object.assign({}, { ...x })),
        actions: state.actions
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Listagem de denúncias'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <Fragment>
            <List
              title={'Listagem de denúncias'}
              columns={state.columns}
              rows={state.data}
              actions={state.actions}
            />
            <Button onClick={() => { history.push('/register-complaint') }} color="primary">
              Cadastrar denúncia
            </Button>
          </Fragment>
      }
    </Content>
  )
}