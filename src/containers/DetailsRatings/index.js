import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function DetailsRatings() {
  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id' },
      { title: 'Título', field: 'title', render: rowData => 'titulo' },
      { title: 'Responsável', field: 'responsible', render: rowData => 'subtitulo' },
      { title: 'Nota', field: 'rating', render: rowData => 'descrição' }
    ],
    data: [],
    actions: []
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({
        columns: state.columns,
        data: data.waterRegisterHistoriesByUser.map(x => Object.assign({}, { ...x })),
        actions: state.actions
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Detalhes de avaliações'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Detalhes de avaliações'}
            columns={state.columns}
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}