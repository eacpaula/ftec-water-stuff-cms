import { gql } from '@apollo/client'

const ADD_DATA = gql`
  mutation AddComplaint(
    $title: String!,
    $subtitle: String!
    $description: String!, 
  ) {
    addComplaint(
      data: {
        title: $title,
        subtitle: $subtitle,
        description: $description
      }
    ) {
      id
    }
  }
`;

const mutations = {
  ADD_DATA
}

export default mutations