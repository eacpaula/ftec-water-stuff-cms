import React, { useState, Fragment } from 'react'
import { useHistory } from 'react-router-dom'
import { CircularProgress, Button } from '@material-ui/core'
import { useQuery } from '@apollo/client'
import { object, string, number } from "yup"

import queries from './queries'
import mutations from './mutations'

import Content from '../../components/Content'
import Form from '../../components/Form'

export default function RegisterComplaint() {
  const history = useHistory()
  const initialState = {
    title: "",
    subtitle: "",
    description: ""
  }

  const schema = object().shape({
    title: string().required("O título é obrigatório!"),
    subtitle: string(),
    description: string().required("A descrição é obrigatória!"),
    water_register_id: number()
	})

  const inputs = [
    {
      type: "text",
      name: "title",
      label: "Título",
      readOnly: true,
      props: {
        placeholder: "Código",
        style: ""
      }
    },
    {
      type: "text",
      name: "subtitle",
      label: "Subtítulo",
      readOnly: true,
      props: {
        placeholder: "Endereço",
      }
    },
    {
      type: "text",
      name: "description",
      label: "Descrição",
      props: {
        placeholder: "Descrição",
      }
    },
    {
      type: "hidden",
      name: "water_register_id"
    },
  ]

  const { loading } = useQuery( queries.GET_DATA, {
    onCompleted: (response) => {
      var data

      if(response && response.waterRegisterByUser) {
        const info = response.waterRegisterByUser;

        data = {
          ...info,
          water_register_id: info.id
        }
      }

      setState({
        ...state,
        data
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [state, setState] = useState({
    inputs,
    schema,
    initialState,
    data: {}
  })

  const mutation = { definition: mutations.ADD_DATA, name: 'addWaterRegisterContact' }

  return (
    <Content title={'Cadastrar denúncia'}>
      { loading ?
          <CircularProgress disableShrink />
        :
        <Fragment>
          <Form
            title="Cadastrar denúncia"
            description=""
            source={state.data}
            inputs={state.inputs}
            schema={state.schema}
            mutation={mutation}
            callback={(success, data) => {}}
            initialState={state.initialState} />
          <Button onClick={() => { history.push('/listing-complaints') }} color="primary">
            Cancelar
          </Button>
        </Fragment>
      }
    </Content>
  )
}