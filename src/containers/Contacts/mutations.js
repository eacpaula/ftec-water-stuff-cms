import { gql } from '@apollo/client'

const DELETE_DATA = gql`
  mutation RemoveContact($id: Int!) {
    removeContact(id: $id) {
      id
    }
  }
`;

const UPDATE_CONTACT_SITUATION = gql`
  mutation UpdateContactSituation(
    $id: Int!
    $user_id: Int!
    $status: Float!
  ) {
    updateContactsituation(
      data: {
        id: $id,
        user_id: $user_id,
        status: $status
      }
    ) {
      id
    }
  }
`;

const mutations = {
    DELETE_DATA,
    UPDATE_CONTACT_SITUATION
}

export default mutations