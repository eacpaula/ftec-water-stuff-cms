import * as React from 'react'
import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import {
  AccountBox,
  ArtTrack,
  Dashboard,
  Email,
  QuestionAnswer,
  SupervisedUserCircle,
  ViewCarousel
} from '@material-ui/icons'
import { Link } from 'react-router-dom'

function ListItemLink(props) {
  const { icon, primary, to } = props;

  const CustomLink = props => <Link to={to} {...props} />;

  return (
    <li>
      <ListItem button component={CustomLink}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  )
}

export default function MenuItems() {
  return (
    <div>
      <ListItem button>
        <ListItemIcon>
          <Dashboard />
        </ListItemIcon>
        <ListItemText primary={'Dashboard'} />
      </ListItem>
      <ListItemLink
        icon={<ViewCarousel />}
        primary={'Banners'}
        to={'/banners'}
      />
      <ListItemLink
        icon={<Email />}
        primary={'Solicitações e Protocolos'}
        to={'/contacts'}
      />
      <ListItemLink
        icon={<ArtTrack />}
        primary={'Nota Públicas'}
        to={'/news'}
      />
      <ListItemLink
        icon={<AccountBox />}
        primary={'Assuntos'}
        to={'/subjects'}
      />
       <ListItemLink
        icon={<SupervisedUserCircle />}
        primary={'Denúncias'}
        to={'/complaints'}
      />
       <ListItemLink
        icon={<SupervisedUserCircle />}
        primary={'Avaliações'}
        to={'/ratings'}
      />
      {/* <ListItemLink
        icon={<SupervisedUserCircle />}
        primary={'Usuários'}
        to={'/users'}
      /> */}
      {/* <ListItemLink
        icon={<QuestionAnswer />}
        primary={'Listagem de previsibilidade de manutenções'}
        to={'/listing-maintenance'}
      /> */}
    </div>
  )
}