import moment from 'moment'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function DetailsMaintenance() {
  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id' },
      { title: 'Total', field: 'title', render: rowData => 'titulo' },
      { title: 'Descrição', field: 'description', render: rowData => 'descrição' },
      { title: 'Localização', field: 'localization', render: rowData => 'COLOCAR MAPS' }
    ],
    data: [],
    actions: []
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({
        columns: state.columns,
        data: data.waterRegisterHistoriesByUser.map(x => Object.assign({}, { ...x })),
        actions: state.actions
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Detalhes de previsibilidade de manutenção'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Detalhes de previsibilidade de manutenção'}
            columns={state.columns}
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}