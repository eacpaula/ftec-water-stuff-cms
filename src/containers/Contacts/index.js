import moment from 'moment'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useLazyQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function Contacts() {
  const [ loading, setLoading ] = useState(false);

  const [loadContacts] = useLazyQuery(queries.GET_DATA, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setState({ 
        ...state,
        data: data.contacts.map(x => Object.assign({}, { ...x, path: 'contacts' })),
      });

      setLoading(false);
    },
		onError: (error) => {
			console.log(error)
      
      setLoading(false);
		}
  })

  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id'},
      { title: 'Assunto', field: 'subject_title', render: rowData => rowData.subject.title },
      { title: 'Assunto Id', field: 'subject_id', hidden: true, render: rowData => rowData.subject.id },
      { title: 'Nome Completo', field: 'fullname'},
      { title: 'Email', field: 'email'},
      { title: 'Celular', field: 'cellphone'},
      { title: 'Mensagem', field: 'message' },
      { title: 'Criado Em', field: 'createdAt', render: rowData => <Fragment> { moment(rowData.createdAt).format('DD/MM/YYYY') } </Fragment> },
    ],
    data: [],
    actions: [
      { 
        action: 'view', 
        title: "Visualizar Solicitação"
      },
    ] 
  })

  useEffect(() => {
    if(state.data.length <= 0){
      setLoading(true)
      loadContacts()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
	}, [state])

  return (
    <Content title={'Solicitações e Protocolos'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Solicitações e Protocolos'}
            columns={state.columns} 
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}