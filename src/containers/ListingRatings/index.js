import moment from 'moment'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function ListingRatings() {
  const [state, setState] = useState({
    columns: [
      { title: 'Descrição', field: 'description', render: data => 'Descrição da avaliação' },
      { title: 'Nota', field: 'rating', render: data => '10'},
    ],
    data: [],
    actions: []
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({
        columns: state.columns,
        data: data.waterRegisterHistoriesByUser.map(x => Object.assign({}, { ...x })),
        actions: state.actions
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Listagem de avaliações'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Listagem de avaliações'}
            columns={state.columns}
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}