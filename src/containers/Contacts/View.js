import React, { useState, useEffect } from 'react'
import { useLocation, useHistory } from 'react-router'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'
import { useLazyQuery, useMutation } from '@apollo/client'
import {
  CircularProgress,
  Select,
  MenuItem,
  Button,
} from "@material-ui/core";
import Alert from '@material-ui/lab/Alert'

import Content from '../../components/Content'

import queries from './queries'
import mutations from './mutations'

import './styles.css'

export default function PublicNote() {
  const history = useHistory()
  const location = useLocation()
  const data = location.state.data
  
  const [ users, setUsers ] = useState([])
  const [ loading, setLoading ] = useState(false)
  const [ status, setStatus ] = useState(null)
  const [ responsable, setResponsable ] = useState(null)
  const [ showMessage, setShowMessage ] = useState(false)

  const handleClick = () => {
    history.goBack()
  }

  const [loadUsers] = useLazyQuery(queries.GET_DATA_USERS, {
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setUsers(data.users);
    },
		onError: (error) => {
			console.log(error)
		}
  })

  const [mutate] = useMutation(mutations.UPDATE_CONTACT_SITUATION, {
    onCompleted: (data) => {
      setShowMessage(true);
      setLoading(false);
    },
    onError: (error) => {
      setShowMessage(true);
      setLoading(false);
    },
  })

  useEffect(() => {
    if(users.length <= 0){
      loadUsers()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
	}, [users])

  const handleUpdateStatus = () => {
    setLoading(true);
    mutate({ variables: {
      "id": parseInt(data.id),
      "user_id": parseInt(responsable),
      "status": parseInt(status),
    } });
  }

  return (
    <Content title={'Solicitações e Protocolos'}>
      <div className='contact-header'>
        <button onClick={handleClick}>
          <ArrowBackIcon className='contact-header-back-history' />
        </button>
        <h3>Assunto: {data.subject.title}</h3>
      </div>
      <section>
        <p>
          <strong>Mensagem</strong>:
          <div dangerouslySetInnerHTML={{ __html: data.message }} />
        </p>
      </section>
      <div className='contact-update-situation'>
        <Select 
          placeholder='Responsável'
          label="Responsável"
          id="responsable" 
          name="responsable"
          onChange={(e) => {
            setResponsable(e.target.value)
          }}
        >
          {users.map(user => (
            <MenuItem value={user.id}>
              {user.username}
            </MenuItem>
          ))}  
        </Select>
        <Select
          placeholder='Status'
          label="Status"
          id="status" 
          name="status"
          onChange={(e) => {
            setStatus(e.target.value)
          }}
        >
          <MenuItem value="1">Não iniciado</MenuItem>
          <MenuItem value="2">Em Andamento</MenuItem>
          <MenuItem value="3">Concluído</MenuItem>
        </Select>
      </div>
      <div className='contact-update-situation-action'>
        <Button onClick={handleUpdateStatus} color="primary">
          {loading && <CircularProgress disableShrink />}
          Atualizar situação
        </Button>
        { showMessage && (
          <Alert
            variant="outlined" 
            severity="success"
          >
            Situação foi atualizada
          </Alert>
        )}
      </div>
    </Content>
  )
}