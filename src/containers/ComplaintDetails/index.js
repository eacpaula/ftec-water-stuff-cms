import moment from 'moment'
import { React, useState, useEffect, Fragment } from 'react'
import { CircularProgress } from '@material-ui/core'
import { useQuery } from '@apollo/client'

import Content from '../../components/Content'
import List from '../../components/List'

import queries from './queries'

export default function ComplaintDetails() {
  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id' },
      { title: 'Título', field: 'title', render: rowData => 'titulo' },
      { title: 'Subtítulo', field: 'subtitle', render: rowData => 'subtitulo' },
      { title: 'Descrição', field: 'description', render: rowData => 'descrição' }
    ],
    data: [],
    actions: []
  })

  const { loading } = useQuery(queries.GET_DATA, {
    onCompleted: (data) => {
      setState({
        columns: state.columns,
        data: data.waterRegisterHistoriesByUser.map(x => Object.assign({}, { ...x })),
        actions: state.actions
      })
    },
		onError: (error) => {
			console.log(error)
		}
  })

  return (
    <Content title={'Detalhes de denúncias'}>
      { loading ?
          <CircularProgress disableShrink />
        :
          <List
            title={'Detalhes de denúncias'}
            columns={state.columns}
            rows={state.data}
            actions={state.actions}
          />
      }
    </Content>
  )
}